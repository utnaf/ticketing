<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/** @testdox UserController */
final class UserTest extends WebTestCase
{

    /**
     * @testdox Any user can see the edit form
     * @dataProvider provideUsernameAndPassword
     */
    public function testUserEdit(string $user, string $password)
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => $user,
            'PHP_AUTH_PW' => $password,
        ]);

        $client->request('GET', '/user/edit');

        $this->assertResponseIsSuccessful();
    }

    /**
     * @testdox Any user can change it's name
     * @dataProvider provideUsernameAndPassword
     */
    public function testUserChangeName(string $user, string $password)
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => $user,
            'PHP_AUTH_PW' => $password,
        ]);

        $client->followRedirects(true);

        $crawler = $client->request('POST', '/user/edit', [
            'user' => [
                'name' => 'New name',
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(UserType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertEquals('New name', $crawler->filter('#user_name')->attr('value'));
    }

    /**
     * @testdox Any user can change it's notification preferences
     * @dataProvider provideUsernameAndPassword
     */
    public function testUserChangeNotificationPreferences(string $user, string $password)
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => $user,
            'PHP_AUTH_PW' => $password,
        ]);

        $client->followRedirects(true);

        $crawler = $client->request('POST', '/user/edit', [
            'user' => [
                'enabledNotificationTypes' => ['1', '2'],
                'name' => 'Name',
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(UserType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertEquals('checked', $crawler->filter('#user_enabledNotificationTypes_1')->attr('checked'));
        $this->assertEquals('checked', $crawler->filter('#user_enabledNotificationTypes_2')->attr('checked'));
    }

    public function provideUsernameAndPassword(): array
    {
        return [
            'ROLE_ADMIN' => ['admin@example.com', 'password'],
            'ROLE_USER' => ['user@example.com', 'password'],
        ];
    }
}