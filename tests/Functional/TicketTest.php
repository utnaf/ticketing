<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use App\Form\CommentType;
use App\Form\EditTicketType;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/** @testdox TicketController */
final class TicketTest extends WebTestCase
{

    /** @testdox Users with just ROLE_USER should see only their created tickets */
    public function testTicketsGetUsers()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'user@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(2, $crawler->filter('#ticket-list tbody tr'));
    }

    /** @testdox Users with ROLE_ADMIN should see all the tickets */
    public function testTicketsGetAdmin()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $crawler = $client->request('GET', '/ticket/');

        $this->assertResponseIsSuccessful();
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(46, $crawler->filter('#ticket-list tbody tr'));
    }

    /**
     * @testdox Every type of user should be able to add a ticket
     * @dataProvider provideUsernameAndPasswords
     */
    public function testTicketAddForm(string $username, string $password)
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => $username,
            'PHP_AUTH_PW' => $password,
        ]);

        $crawler = $client->request('GET', '/ticket/new');

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertSelectorTextSame('h1', 'Create Ticket');
        $this->assertCount(4, $crawler->filter('#add_ticket_assignedTo option'));
        $this->assertSelectorExists('#add_ticket_comments_0_content');
        $this->assertSelectorTextSame('#submit_ticket', 'Save');
    }

    public function provideUsernameAndPasswords(): array
    {
        return [
            'ROLE_USER' => ['user@example.com', 'password'],
            'ROLE_ADMIN' => ['admin@example.com', 'password'],
        ];
    }

    /** @testdox User with just ROLE_USER should see details of their tickets */
    public function testTicketUserShowOk()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'user@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('GET', '/ticket/1/edit');

        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    /** @testdox User with just ROLE_USER should not see details of other users tickets */
    public function testTicketUserShowKo()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'user@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('GET', '/ticket/3/edit');

        $this->assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    /**
     * @testdox User with ROLE_ADMIN should see all tickets
     * @dataProvider provideTicketIds
     */
    public function testTicketAdminShowOk(int $ticketId)
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('GET', '/ticket/' . $ticketId . '/edit');

        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function provideTicketIds()
    {
        return [
            'Ticket #1' => [1],
            'Ticket #2' => [2],
            'Ticket #3' => [3],
            'Ticket #4' => [4]
        ];
    }

    /** @testdox User with just ROLE_USER should not assign ticket */
    public function testTicketUserEditKo()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'user@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('POST', '/ticket/3/edit', [
            'edit_ticket' => [
                'assignTo' => 1,
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(EditTicketType::TOKEN_ID)
            ]
        ]);

        $this->assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    /** @testdox User with ROLE_ADMIN should assign new tickets */
    public function testTicketAdminEditStatusNewOk()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('POST', '/ticket/1/edit', [
            'edit_ticket' => [
                'assignedTo' => 2,
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(EditTicketType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertResponseRedirects('/ticket/');
    }

    /** @testdox User with ROLE_ADMIN should reassign their assigned tickets */
    public function testTicketAdminEditReassignedOk()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('POST', '/ticket/4/edit', [
            'edit_ticket' => [
                'assignedTo' => 2,
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(EditTicketType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertResponseRedirects('/ticket/');
    }

    /**
     * @testdox User with ROLE_ADMIN should not reassign any ticket already assigned to someone else
     * @dataProvider provideAdminAlreadyAssignedTicketIds
     */
    public function testTicketAdminEditReassignedKo(int $id)
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('POST', '/ticket/4/edit', [
            'edit_ticket' => [
                'assignedTo' => 2,
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(EditTicketType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertResponseRedirects('/ticket/');
    }

    public function provideAdminAlreadyAssignedTicketIds()
    {
        return [
            'Admin ticket assigned to another admin' => [5],
            'Another admin ticket assigned to another admin' => [6],
        ];
    }

    /** @testdox User with just ROLE_USER should only comment their tickets */
    public function testTicketUserCommentOk()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'user@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->followRedirects(true);

        $crawler = $client->request('POST', '/ticket/1/comment', [
            'comment' => [
                'content' => 'This is a comment!',
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(CommentType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertCount(2, $crawler->filter('.card'));
    }

    /** @testdox User with just ROLE_USER should not comment other tickets */
    public function testTicketUserCommentKo()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'user@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('POST', '/ticket/3/comment', [
            'comment' => [
                'content' => 'This is a comment!',
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(CommentType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    /** @testdox User with ROLE_ADMIN should comment open tickets */
    public function testTicketAdminCommentOkOpen()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->followRedirects(true);

        $crawler = $client->request('POST', '/ticket/1/comment', [
            'comment' => [
                'content' => 'This is a comment!',
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(CommentType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertCount(2, $crawler->filter('.card'));
    }

    /** @testdox User with ROLE_ADMIN should comment their assigned tickets */
    public function testTicketAdminCommentOkAssigned()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->followRedirects(true);

        $crawler = $client->request('POST', '/ticket/4/comment', [
            'comment' => [
                'content' => 'This is a comment!',
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(CommentType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertCount(2, $crawler->filter('.card'));
    }

    /** @testdox User with ROLE_ADMIN should not comment tickets not assigned to them */
    public function testTicketAdminCommentKo()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('POST', '/ticket/6/comment', [
            'comment' => [
                'content' => 'This is a comment!',
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(CommentType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    /** @testdox User with ROLE_ADMIN should get and open ticket assigned if their the first admin to comment */
    public function testTicketAdminFirstCommentAssign()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->followRedirects(true);

        $crawler = $client->request('POST', '/ticket/1/comment', [
            'comment' => [
                'content' => 'This is a comment!',
                '_token' => self::$container->get('security.csrf.token_manager')
                    ->getToken(CommentType::TOKEN_ID)->getValue()
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertSame(
            'The Admin',
            $crawler->filter('#edit_ticket_assignedTo [selected]')->first()->text()
        );
    }

    /** @testdox User with just ROLE_USER should close their tickets */
    public function testTicketUserCloseTicketOk()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'user@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->followRedirects(true);

        $client->request('PATCH', '/ticket/1', [
            '_token' => self::$container->get('security.csrf.token_manager')
                ->getToken('close_ticket_token')->getValue()
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextSame('.badge.text-uppercase', 'closed');
    }

    /** @testdox User with just ROLE_USER should not close other tickets */
    public function testTicketUserCloseTicketKo()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'user@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->followRedirects(true);

        $client->request('PATCH', '/ticket/3', [
            '_token' => self::$container->get('security.csrf.token_manager')
                ->getToken('close_ticket_token')->getValue()
        ]);

        $this->assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    /** @testdox User with ROLE_ADMIN should close open ticket */
    public function testTicketAdminCloseOpenTicket()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->followRedirects(true);

        $client->request('PATCH', '/ticket/1', [
            '_token' => self::$container->get('security.csrf.token_manager')
                ->getToken('close_ticket_token')->getValue()
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextSame('.badge.text-uppercase', 'closed');
    }

    /** @testdox User with ROLE_ADMIN should close his assigned tickets */
    public function testTicketAdminCloseHisAssignedTicket()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->followRedirects(true);

        $client->request('PATCH', '/ticket/2', [
            '_token' => self::$container->get('security.csrf.token_manager')
                ->getToken('close_ticket_token')->getValue()
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextSame('.badge.text-uppercase', 'closed');
    }

    /** @testdox User with ROLE_ADMIN should not close other admins assigned tickets */
    public function testTicketAdminCloseTicketKo()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('PATCH', '/ticket/6', [
            '_token' => self::$container->get('security.csrf.token_manager')
                ->getToken('close_ticket_token')->getValue()
        ]);

        $this->assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }
}