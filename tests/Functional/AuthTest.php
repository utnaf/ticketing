<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/** @testdox Authentication */
final class AuthTest extends WebTestCase
{
    /**
     * @testdox Login should log users in
     */
    public function testLogin()
    {
        $client = self::createClient();

        $client->request('POST', '/login', [
            'email' => 'user@example.com',
            'password' => 'password',
            '_csrf_token' => self::$container->get('security.csrf.token_manager')
                ->getToken('authenticate')->getValue()
        ]);

        $this->assertResponseRedirects('/');
    }

    /**
     * @testdox Logout should log users out :O
     */
    public function testLogout()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'admin@example.com',
            'PHP_AUTH_PW' => 'password',
        ]);

        $client->request('GET', '/logout');

        $this->assertResponseRedirects('http://localhost/');
    }

    /**
     * @testdox Anonymous users should be redirected to login
     * @dataProvider provideMethodsAndPaths
     */
    public function testRoutesAreProtected(string $method, string $path)
    {
        $client = self::createClient();

        $client->request($method, $path);

        $this->assertResponseRedirects('/login');
    }

    public function provideMethodsAndPaths(): array
    {
        return [
            'GET: ticket/' => ['GET', '/ticket/'],
            'GET: ticket/new' => ['GET', '/ticket/new'],
            'POST: ticket/new' => ['POST', '/ticket/new'],
            'GET: ticket/1/edit' => ['GET', '/ticket/1/edit'],
            'POST: ticket/1/edit' => ['POST', '/ticket/1/edit'],
            'POST: ticket/1/comment' => ['POST', '/ticket/1/comment'],
            'PATCH: ticket/1' => ['PATCH', '/ticket/1'],
            'GET: user/edit' => ['GET', '/user/edit'],
        ];
    }
}