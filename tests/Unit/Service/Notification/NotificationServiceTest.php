<?php
declare(strict_types=1);

namespace App\Tests\Unit\Service\Notification;

use App\Entity\NotificationType;
use App\Entity\User;
use App\Factory\NotificationFactory;
use App\Service\Notification\NotificationHandlerInterface;
use App\Service\Notification\NotificationService;
use App\ValueObject\NotificationMessage;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/** @testdox Given a NotificationService */
final class NotificationServiceTest extends TestCase
{

    /** @testdox and a User with 2 configured types When notify() is called Then it should use all 2 types */
    public function testShouldSendCorrectTypes()
    {
        $smsType = new NotificationType();
        $smsType->setName('sms');

        $pushType = new NotificationType();
        $pushType->setName('push');

        $smsNotificationHandler = $this->prophesize(NotificationHandlerInterface::class);
        $smsNotificationHandler->send(Argument::type(NotificationMessage::class))->shouldBeCalledOnce();
        $pushNotificationHandler = $this->prophesize(NotificationHandlerInterface::class);
        $pushNotificationHandler->send(Argument::type(NotificationMessage::class))->shouldBeCalledOnce();

        $factory = new NotificationFactory();
        $factory->addType('sms', $smsNotificationHandler->reveal());
        $factory->addType('push', $pushNotificationHandler->reveal());

        $logger = $this->prophesize(LoggerInterface::class);
        $logger->alert(Argument::type('string'))->shouldNotBeCalled();

        $notificationService = new NotificationService(
            $factory,
            $logger->reveal()
        );

        $user = new User();
        $user->setName('User');
        $user->addEnabledNotificationType($smsType);
        $user->addEnabledNotificationType($pushType);

        $message = new NotificationMessage($user, 'The best message you can get.');
        $notificationService->send($message);
    }

    /** @testdox and a User with 1 non-existent type When notify() is called Then it should log alert */
    public function testShouldGiveAlert()
    {
        $smsType = new NotificationType();
        $smsType->setName('sms');

        $factory = new NotificationFactory();

        $logger = $this->prophesize(LoggerInterface::class);
        $logger->alert('System was not able to notify User [ID 0] user with notification method "sms"')
            ->shouldBeCalledOnce();

        $notificationService = new NotificationService(
            $factory,
            $logger->reveal()
        );

        $user = new User();
        $user->setName('User');
        $user->addEnabledNotificationType($smsType);

        $message = new NotificationMessage($user, 'The best message you can get.');
        $notificationService->send($message);
    }
}