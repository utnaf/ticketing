<?php
declare(strict_types=1);

namespace App\Tests\Unit\EventListener;

use App\Entity\Comment;
use App\Entity\Ticket;
use App\Entity\User;
use App\EventListener\NewCommentListener;
use App\Service\Notification\NotificationService;
use App\ValueObject\NotificationMessage;
use Doctrine\ORM\Event\LifecycleEventArgs;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/** @testdox Given a NewCommentListener */
class NewCommentListenerTest extends TestCase
{
    /** @testdox When a new Comment by the author is created and the ticket is assigned Then should notify the assigned user */
    public function testNotifyAssignedAdmin()
    {
        $admin = new User();
        $admin->setRoles(['ROLE_ADMIN']);

        $user = new User();
        $user->setName('Johnny B. Goode');

        $notificationService = $this->prophesize(NotificationService::class);
        $notificationService->send(
            new NotificationMessage(
                $admin,
                'New message on ticket #0 by Johnny B. Goode.'
            )
        )
            ->shouldBeCalledTimes(1);

        $newCommentListener = new NewCommentListener(
            $notificationService->reveal()
        );

        $ticket = new Ticket();
        $ticket->setCreatedBy($user);
        $ticket->setAssignedTo($admin);

        $comment1 = new Comment();
        $comment1->setCreatedBy($user);
        $comment1->setContent('First comment');
        $ticket->addComment($comment1);

        $comment2 = new Comment();
        $comment2->setCreatedBy($user);
        $comment2->setContent('Second comment');
        $ticket->addComment($comment2);

        $eventArgs = $this->prophesize(LifecycleEventArgs::class);
        $eventArgs->getEntity()->willReturn($comment2);

        $newCommentListener->postPersist($eventArgs->reveal());
    }

    /** @testdox When the first Comment by the author is created and the ticket is assigned Then should not notify the assigned user */
    public function testNotifyAssignedAdminKoFirstComment()
    {
        $admin = new User();
        $admin->setRoles(['ROLE_ADMIN']);

        $user = new User();
        $user->setName('Johnny B. Goode');

        $notificationService = $this->prophesize(NotificationService::class);
        $notificationService->send(Argument::type(NotificationMessage::class))
            ->shouldNotBeCalled();

        $newCommentListener = new NewCommentListener(
            $notificationService->reveal()
        );

        $ticket = new Ticket();
        $ticket->setCreatedBy($user);
        $ticket->setAssignedTo($admin);

        $comment1 = new Comment();
        $comment1->setCreatedBy($user);
        $comment1->setContent('First comment');
        $ticket->addComment($comment1);

        $eventArgs = $this->prophesize(LifecycleEventArgs::class);
        $eventArgs->getEntity()->willReturn($comment1);

        $newCommentListener->postPersist($eventArgs->reveal());
    }

    /** @testdox When a new Comment by the author is created and the ticket is not assigned Then should not notify */
    public function testNotifyAssignedAdminKoNotAssigned()
    {
        $user = new User();
        $user->setName('Johnny B. Goode');

        $notificationService = $this->prophesize(NotificationService::class);
        $notificationService->send()
            ->shouldNotBeCalled();

        $newCommentListener
            = new NewCommentListener(
            $notificationService->reveal()
        );

        $ticket = new Ticket();
        $ticket->setCreatedBy($user);

        $comment1 = new Comment();
        $comment1->setCreatedBy($user);
        $comment1->setContent('First comment');
        $ticket->addComment($comment1);

        $comment2 = new Comment();
        $comment2->setCreatedBy($user);
        $comment2->setContent('Second comment');
        $ticket->addComment($comment2);

        $eventArgs = $this->prophesize(LifecycleEventArgs::class);
        $eventArgs->getEntity()->willReturn($comment2);

        $newCommentListener->postPersist($eventArgs->reveal());
    }

    /** @testdox When a new Comment by the assigned admin is created and the ticket is assigned Then should  notify the owner */
    public function testNotifyAssignedAdminKoNotAuthor()
    {
        $admin = new User();
        $admin->setName('God Itself');
        $admin->setRoles(['ROLE_ADMIN']);

        $user = new User();
        $user->setName('Johnny B. Goode');

        $notificationService = $this->prophesize(NotificationService::class);
        $notificationService->send(
            new NotificationMessage(
                $user,
                'New message on ticket #0 by God Itself.'
            )
        )
            ->shouldBeCalledOnce();

        $newCommentListener = new NewCommentListener(
            $notificationService->reveal()
        );

        $ticket = new Ticket();
        $ticket->setCreatedBy($user);
        $ticket->setAssignedTo($admin);

        $comment1 = new Comment();
        $comment1->setCreatedBy($user);
        $comment1->setContent('First comment');
        $ticket->addComment($comment1);

        $comment2 = new Comment();
        $comment2->setCreatedBy($admin);
        $comment2->setContent('Second comment');
        $ticket->addComment($comment2);

        $eventArgs = $this->prophesize(LifecycleEventArgs::class);
        $eventArgs->getEntity()->willReturn($comment2);

        $newCommentListener->postPersist($eventArgs->reveal());
    }
}