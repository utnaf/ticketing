<?php
declare(strict_types=1);

namespace App\Tests\Unit\EventListener;

use App\Entity\Ticket;
use App\Entity\User;
use App\EventListener\CreatedTicketListener;
use App\Repository\UserRepository;
use App\Service\Notification\NotificationService;
use App\Tests\Stub\EntityManagerStub;
use App\ValueObject\NotificationMessage;
use Doctrine\ORM\Event\LifecycleEventArgs;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/** @testdox Give a CreatedTicketListene */
class CreatedTicketListenerTest extends TestCase
{
    /** @testdox When a new Ticket is inserted Then should notify all the admin */
    public function testNotifyAdmins()
    {
        $admin1 = new User();
        $admin1->setRoles(['ROLE_ADMIN']);

        $admin2 = new User();
        $admin2->setRoles(['ROLE_ADMIN']);

        $users = [
            $admin1, new User(), $admin2, new User(), new User()
        ];

        $userRepository = $this->prophesize(UserRepository::class);
        $userRepository->findAll()->shouldBeCalledOnce()
            ->willReturn($users);

        $notificationService = $this->prophesize(NotificationService::class);
        $notificationService->send(Argument::type(NotificationMessage::class))
            ->shouldBeCalledTimes(2);

        $createdTicketListener = new CreatedTicketListener(
            $notificationService->reveal()
        );

        $user = new User();
        $user->setName('Johnny B. Goode');

        $ticket = new Ticket();
        $ticket->setCreatedBy($user);

        $eventArgs = $this->prophesize(LifecycleEventArgs::class);
        $eventArgs->getEntity()->willReturn($ticket);
        $eventArgs->getEntityManager()->willReturn(new EntityManagerStub($userRepository->reveal()));

        $createdTicketListener->postPersist($eventArgs->reveal());
    }
}