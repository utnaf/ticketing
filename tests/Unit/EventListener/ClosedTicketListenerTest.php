<?php
declare(strict_types=1);

namespace App\Tests\Unit\EventListener;

use App\Entity\Ticket;
use App\Entity\User;
use App\EventListener\ClosedTicketListener;
use App\Repository\UserRepository;
use App\Service\Notification\NotificationService;
use App\Tests\Stub\EntityManagerStub;
use App\ValueObject\NotificationMessage;
use Doctrine\ORM\Event\LifecycleEventArgs;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/** @testdox Given a ClosedTicketListener */
class ClosedTicketListenerTest extends TestCase
{
    /** @testdox When a ticket is closed without anyone assigned Then it should notify all admin */
    public function testShouldNotifyAllAdmin()
    {
        $admin1 = new User();
        $admin1->setRoles(['ROLE_ADMIN']);

        $admin2 = new User();
        $admin2->setRoles(['ROLE_ADMIN']);

        $users = [
            $admin1, new User(), $admin2, new User(), new User()
        ];

        $userRepository = $this->prophesize(UserRepository::class);
        $userRepository->findAll()->shouldBeCalledOnce()
            ->willReturn($users);

        $notificationService = $this->prophesize(NotificationService::class);
        $notificationService->send(Argument::type(NotificationMessage::class))
            ->shouldBeCalledTimes(2);

        $closedTicketListener = new ClosedTicketListener(
            $notificationService->reveal()
        );

        $user = new User();
        $user->setName('Johnny B. Goode');

        $ticket = new Ticket();
        $ticket->setCreatedBy($user);
        $ticket->setStatus(Ticket::STATUS_CLOSED);
        $ticket->setClosedBy($user);
        $ticket->setAssignedTo(null);

        $eventArgs = $this->prophesize(LifecycleEventArgs::class);
        $eventArgs->getEntity()->willReturn($ticket);
        $eventArgs->getEntityManager()->willReturn(new EntityManagerStub($userRepository->reveal()));

        $closedTicketListener->postUpdate($eventArgs->reveal());
    }

    /** @testdox When a ticket is closed by the opening user and assigned Then it should notify the assigned user */
    public function testShouldNotifyAssigned()
    {
        $admin = new User();
        $admin->setRoles(['ROLE_ADMIN']);

        $user = new User();
        $user->setName('Johnny B. Goodie');

        $userRepository = $this->prophesize(UserRepository::class);
        $userRepository->findAll()->shouldNotBeCalled();

        $notificationService = $this->prophesize(NotificationService::class);
        $notificationService->send(new NotificationMessage(
            $admin,
            'The ticket #0 have been closed by Johnny B. Goode.'
        ))
            ->shouldBeCalledOnce();

        $closedTicketListener = new ClosedTicketListener(
            $notificationService->reveal()
        );

        $user = new User();
        $user->setName('Johnny B. Goode');

        $ticket = new Ticket();
        $ticket->setCreatedBy($user);
        $ticket->setAssignedTo($admin);
        $ticket->setStatus(Ticket::STATUS_CLOSED);
        $ticket->setClosedBy($user);

        $eventArgs = $this->prophesize(LifecycleEventArgs::class);
        $eventArgs->getEntity()->willReturn($ticket);
        $eventArgs->getEntityManager()->willReturn(new EntityManagerStub($userRepository->reveal()));

        $closedTicketListener->postUpdate($eventArgs->reveal());
    }

    /** @testdox When a ticket is closed by assigned user Then it should notify the opening user */
    public function testShouldNotifyOpening()
    {
        $admin = new User();
        $admin->setName('God Itself');
        $admin->setRoles(['ROLE_ADMIN']);

        $user = new User();
        $user->setName('Johnny B. Goode');

        $userRepository = $this->prophesize(UserRepository::class);
        $userRepository->findAll()->shouldNotBeCalled();

        $notificationService = $this->prophesize(NotificationService::class);
        $notificationService->send(new NotificationMessage(
            $user,
            'The ticket #0 have been closed by God Itself.'
        ))
            ->shouldBeCalledOnce();

        $closedTicketListener = new ClosedTicketListener(
            $notificationService->reveal()
        );

        $user = new User();
        $user->setName('Johnny B. Goode');

        $ticket = new Ticket();
        $ticket->setCreatedBy($user);
        $ticket->setAssignedTo($admin);
        $ticket->setStatus(Ticket::STATUS_CLOSED);
        $ticket->setClosedBy($admin);

        $eventArgs = $this->prophesize(LifecycleEventArgs::class);
        $eventArgs->getEntity()->willReturn($ticket);
        $eventArgs->getEntityManager()->willReturn(new EntityManagerStub($userRepository->reveal()));

        $closedTicketListener->postUpdate($eventArgs->reveal());
    }
}