<?php
declare(strict_types=1);

namespace App\Tests\Unit\Factory;

use App\Entity\NotificationType;
use App\Factory\NotificationFactory;
use App\Service\Notification\NotificationHandlerInterface;
use PHPUnit\Framework\TestCase;

/** @testdox Given one NotificationFactory */
final class NotificationFactoryTest extends TestCase
{

    /** @testdox with 2 mapped handlers When asked for a notification type handler Then it returns it */
    public function testReturnsCorrectInstance()
    {
        $notification1 = $this->prophesize(NotificationHandlerInterface::class);
        $notification2 = $this->prophesize(NotificationHandlerInterface::class);

        $factory = new NotificationFactory();

        $notificationOneRevealed = $notification1->reveal();
        $notificationTwoRevealed = $notification2->reveal();

        $factory->addType('sms', $notificationOneRevealed);
        $factory->addType('push', $notificationTwoRevealed);

        $notificationType = new NotificationType();
        $notificationType->setName('sms');

        $this->assertSame(
            $notificationOneRevealed,
            $factory->getByNotificationType($notificationType)
        );

        $notificationType->setName('push');
        $this->assertSame(
            $notificationTwoRevealed,
            $factory->getByNotificationType($notificationType)
        );
    }

    /**
     * @testdox with 2 mapped handlers When asked for a non mapped notification type handler Then it should throw an exception
     * @expectedException \Exception
     * @expectedExceptionMessage Unable to find a notification handler for "mail" type
     */
    public function testException()
    {
        $notification1 = $this->prophesize(NotificationHandlerInterface::class);
        $notification2 = $this->prophesize(NotificationHandlerInterface::class);

        $factory = new NotificationFactory();

        $factory->addType('sms', $notification1->reveal());
        $factory->addType('push', $notification2->reveal());

        $notificationType = new NotificationType();
        $notificationType->setName('mail');

        $factory->getByNotificationType($notificationType);
    }
}