BASE_DOCKER_COMPOSE = docker-compose

up:
	$(BASE_DOCKER_COMPOSE) up --force-recreate -d 
.PHONY: up

init:
	make composer install \
	&& make composer reset-db \
	&& make composer init-test-db
.PHONY: init

test:
	$(BASE_DOCKER_COMPOSE) exec -u $(id -u):$(id -g) -e HOME=/tmp/ ticketing_web php bin/phpunit --testdox
.PHONY: test

notifications:
	$(BASE_DOCKER_COMPOSE) exec -u $(id -u):$(id -g) -e HOME=/tmp/ ticketing_web tail -f var/log/notifications.log
.PHONY: notifications

kill:
	$(BASE_DOCKER_COMPOSE) kill
.PHONY: kill

build:
	$(BASE_DOCKER_COMPOSE) stop \
	&& $(BASE_DOCKER_COMPOSE) rm -f \
	&& $(BASE_DOCKER_COMPOSE) pull \
	&& $(BASE_DOCKER_COMPOSE) build --no-cache
.PHONY: build

composer:
	$(BASE_DOCKER_COMPOSE) exec -u $(id -u):$(id -g) -e HOME=/tmp/ ticketing_web composer $(filter-out $@,$(MAKECMDGOALS))
.PHONY: composer

sh:
	$(BASE_DOCKER_COMPOSE) exec -u $(id -u):$(id -g) -e HOME=/tmp/ ticketing_web bash
.PHONY: sh

rm:
	$(BASE_DOCKER_COMPOSE) rm -f
.PHONY: rm

reset: kill rm up
.PHONY: reset

%:
	@:
