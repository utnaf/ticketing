# Ticketing

Il progetto usa Docker, e un Makefile per interagire più facilmente con i container.

Per inizializzare il progetto:
```bash
make up
make init
```

Verrà già inizializzato il DB con varie utenze random più due fisse:

- User `admin@example.com`, psw `password` con ruolo Admin
- User `user@example.com`, psw `password` con ruolo Utente

L'applicazione si trova su [localhost](http://localhost) oppure si può configurare l'host `ticketing.local` sul proprio file host. Il dominio è già configurato sull'apache dell'immagine Docker.

Per far girare i test:
```bash
make test
```

Per vedere le notifiche che vengono "inviate":
```bash
make notifications
```
*Attenzione*: il file di notifiche viene creato dopo il primo invio di una notifica, potrebbe perciò non essere presente all'avvio.

Altri comandi utili
```bash
make up # avvia i container
make kill # spegne i container
make sh # si connette alla shell del container
make composer [cmd] # lancia un comando di composer sul container
```

Per altre problematiche scrivetemi pure a `utnaf.dev@gmail.com`