<?php
declare(strict_types=1);

namespace App\Service\Notification;

use Psr\Log\LoggerInterface;

abstract class AbstractNotificationHandlerHandler implements NotificationHandlerInterface
{
    /** @var LoggerInterface */
    protected $logger;

    public function __construct(LoggerInterface $notificationLogger)
    {
        $this->logger = $notificationLogger;
    }
}