<?php
declare(strict_types=1);

namespace App\Service\Notification;

use App\Entity\NotificationType;
use App\Factory\NotificationFactory;
use App\ValueObject\NotificationMessage;
use Psr\Log\LoggerInterface;

class NotificationService
{
    /** @var NotificationFactory */
    private $factory;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        NotificationFactory $factory,
        LoggerInterface $logger
    )
    {
        $this->factory = $factory;
        $this->logger = $logger;
    }

    public function send(NotificationMessage $message): void
    {
        $enabledNotificationTypes = $message->getUser()->getEnabledNotificationTypes();

        $enabledNotificationTypes->forAll(function (int $_i, NotificationType $notificationType) use ($message) {
            try {
                $this->factory
                    ->getByNotificationType($notificationType)
                    ->send($message);
            } catch (\Exception $e) {
                $this->logger->alert(
                    sprintf(
                        'System was not able to notify %s [ID %d] user with notification method "%s"',
                        $message->getUser()->getName(),
                        $message->getUser()->getId(),
                        $notificationType->getName()
                    )
                );

                // we won't stop notification chain
            }

            return true;
        });
    }
}