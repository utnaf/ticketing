<?php
declare(strict_types=1);

namespace App\Service\Notification;

use App\ValueObject\NotificationMessage;

class PushNotificationHandler extends AbstractNotificationHandlerHandler
{

    public function send(NotificationMessage $message)
    {
        $this->logger->info(
            sprintf(
                'Sent Push Notification to %s with text "%s"',
                $message->getUser()->getName(),
                $message->getContent()
            )
        );
    }
}