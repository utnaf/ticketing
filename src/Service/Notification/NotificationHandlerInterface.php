<?php
declare(strict_types=1);

namespace App\Service\Notification;

use App\ValueObject\NotificationMessage;

interface NotificationHandlerInterface
{

    public function send(NotificationMessage $message);

}