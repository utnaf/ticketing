<?php
declare(strict_types=1);

namespace App\ValueObject;

use App\Entity\User;

class NotificationMessage
{
    /** @var User */
    private $user;
    /** @var string */
    private $message;

    public function __construct(User $user, string $message)
    {
        $this->user = $user;
        $this->message = $message;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getContent(): string
    {
        return $this->message;
    }
}