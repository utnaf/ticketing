<?php
declare(strict_types=1);

namespace App\Factory;

use App\Entity\NotificationType;
use App\Service\Notification\NotificationHandlerInterface;

class NotificationFactory
{
    /** @var NotificationHandlerInterface[] */
    private $notifications = [];

    public function addType(string $type, NotificationHandlerInterface $notification): void
    {
        $this->notifications[$type] = $notification;
    }

    /** @throws \Exception */
    public function getByNotificationType(NotificationType $notificationType): NotificationHandlerInterface
    {
        if (!isset($this->notifications[$notificationType->getName()])) {
            throw new \Exception(
                sprintf(
                    'Unable to find a notification handler for "%s" type',
                    $notificationType->getName()
                )
            );
        }

        return $this->notifications[$notificationType->getName()];
    }
}