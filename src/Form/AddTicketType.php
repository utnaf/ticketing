<?php

namespace App\Form;

use App\Entity\Ticket;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddTicketType extends EditTicketType
{
    public const TOKEN_ID = 'add_ticket_token';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('comments', CollectionType::class, [
                'entry_type' => CommentType::class
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
            'csrf_token_id' => static::TOKEN_ID,
            'attr' => [
                'id' => 'add_ticket_form'
            ]
        ]);
    }
}
