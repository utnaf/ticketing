<?php

namespace App\Form;

use App\Entity\NotificationType;
use App\Entity\User;
use App\Repository\NotificationTypeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public const TOKEN_ID = 'edit_user_token';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'row_attr' => [
                    'class' => 'form-group'
                ],
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => true
            ])
            ->add('enabledNotificationTypes', EntityType::class, [
                'attr' => [
                    'class' => 'form-check'
                ],
                'label_attr' => [
                    'class' => 'form-check-label'
                ],
                'class' => NotificationType::class,
                'expanded' => true,
                'multiple' => true,
                'query_builder' => function (NotificationTypeRepository $repository) {
                    return $repository->createQueryBuilder('n')
                        ->orderBy('n.mandatory', 'DESC');
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_token_id' => static::TOKEN_ID,
            'attr' => [
                'id' => 'edit_user_form'
            ]
        ]);
    }
}
