<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Ticket;
use App\Form\AddTicketType;
use App\Form\CommentType;
use App\Form\EditTicketType;
use App\Repository\TicketRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ticket")
 */
class TicketController extends AbstractController
{
    /**
     * @Route("/", name="ticket_index", methods={"GET"})
     */
    public function index(TicketRepository $ticketRepository): Response
    {
        $user = $this->getUser();

        $selectCriteria = [];

        if (!$this->isGranted('ROLE_ADMIN', $user)) {
            $selectCriteria['createdBy'] = $user;
        }

        return $this->render('ticket/index.html.twig', [
            'tickets' => $ticketRepository->findBy($selectCriteria)
        ]);
    }

    /**
     * @Route("/new", name="ticket_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $ticket = new Ticket();
        $ticket->addComment(new Comment());

        $form = $this->createForm(AddTicketType::class, $ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ticket->setCreatedBy($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ticket);
            $entityManager->flush();

            return $this->redirectToRoute('ticket_index');
        }

        return $this->render('ticket/new.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ticket_show", methods={"GET"})
     * @IsGranted("show", subject="ticket", message="You can't see this ticket")
     */
    public function show(Ticket $ticket): Response
    {
        $form = $this->createForm(EditTicketType::class, $ticket);

        return $this->render('ticket/edit.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ticket_edit", methods={"POST"})
     * @IsGranted("edit", subject="ticket", message="You can't edit this ticket")
     */
    public function edit(Request $request, Ticket $ticket): Response
    {
        $form = $this->createForm(EditTicketType::class, $ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ticket_index');
        }

        return $this->render('ticket/edit.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/comment", name="ticket_add_comment", methods={"POST"})
     * @IsGranted("comment", subject="ticket", message="You can't comment on this ticket")
     */
    public function comment(Request $request, Ticket $ticket): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setCreatedBy($this->getUser());
            $ticket->addComment($comment);
            $this->getDoctrine()->getManager()->persist($ticket);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ticket_edit', ['id' => $ticket->getId()]);
        }

        return $this->render('ticket/edit.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ticket_patch", methods={"PATCH"})
     * @IsGranted("close", subject="ticket", message="You can't close this ticket")
     */
    public function patch(Request $request, Ticket $ticket): Response
    {
        if ($this->isCsrfTokenValid('close_ticket_token', $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $ticket->setStatus(Ticket::STATUS_CLOSED);
            $ticket->setClosedBy($this->getUser());
            $entityManager->persist($ticket);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ticket_show', ['id' => $ticket->getId()]);
    }
}
