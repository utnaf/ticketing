<?php
declare(strict_types=1);

namespace App\DependencyInjection\Compiler;

use App\Factory\NotificationFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class NotificationTypesCompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        $ticketSubject = $container->getDefinition(NotificationFactory::class);

        $taggedTicketObservers = $container->findTaggedServiceIds('app.notification.type');

        foreach ($taggedTicketObservers as $id => $tags) {
            $ticketSubject->addMethodCall('addType', [$tags[0]['type'], new Reference($id)]);
        }
    }
}