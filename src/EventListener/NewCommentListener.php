<?php
declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Comment;
use App\Entity\User;
use App\Service\Notification\NotificationService;
use App\ValueObject\NotificationMessage;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class NewCommentListener implements EventSubscriber
{
    /** @var NotificationService */
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist
        ];
    }

    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();

        if (!$entity instanceof Comment) {
            return;
        }

        if (
            $entity->getTicket()->getComments()->count() <= 1
            || !$entity->getTicket()->getAssignedTo() instanceof User
        ) {
            return;
        }

        if ($entity->getCreatedBy() === $entity->getTicket()->getCreatedBy()) {
            $notifiedUser = $entity->getTicket()->getAssignedTo();
        } else {
            $notifiedUser = $entity->getTicket()->getCreatedBy();
        }

        $this->notificationService->send(
            new NotificationMessage(
                $notifiedUser,
                sprintf(
                    'New message on ticket #%d by %s.',
                    $entity->getTicket()->getId(),
                    $entity->getCreatedBy()->getName()
                )
            )
        );
    }
}