<?php
declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Ticket;
use App\Entity\User;
use App\Service\Notification\NotificationService;
use App\ValueObject\NotificationMessage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class ClosedTicketListener implements EventSubscriber
{
    /** @var NotificationService */
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate
        ];
    }

    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();

        if (
            !$entity instanceof Ticket
            || $entity->getStatus() !== Ticket::STATUS_CLOSED
        ) {
            return;
        }

        if (!$entity->getAssignedTo() instanceof User) {
            $users = $eventArgs->getEntityManager()->getRepository(User::class)->findAll();

            (new ArrayCollection($users))
                ->filter(function (User $user) {
                    return $user->hasRole('ROLE_ADMIN');
                })
                ->forAll(function (int $_i, User $user) use ($entity) {
                    $this->notify($user, $entity);

                    return true;
                });

        } else if ($entity->getClosedBy() === $entity->getCreatedBy()) {
            $this->notify($entity->getAssignedTo(), $entity);
        } else if ($entity->getClosedBy() === $entity->getAssignedTo()) {
            $this->notify($entity->getCreatedBy(), $entity);
        }
    }

    private function notify(User $user, Ticket $ticket)
    {
        $this->notificationService->send(
            new NotificationMessage(
                $user,
                sprintf(
                    'The ticket #%d have been closed by %s.',
                    $ticket->getId(),
                    $ticket->getClosedBy()->getName()
                )
            )
        );
    }
}