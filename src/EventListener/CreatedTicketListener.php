<?php
declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Ticket;
use App\Entity\User;
use App\Service\Notification\NotificationService;
use App\ValueObject\NotificationMessage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class CreatedTicketListener implements EventSubscriber
{
    /** @var NotificationService */
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist
        ];
    }

    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();

        if (!$entity instanceof Ticket) {
            return;
        }

        $users = $eventArgs->getEntityManager()->getRepository(User::class)->findAll();

        (new ArrayCollection($users))
            ->filter(function (User $user) {
                return $user->hasRole('ROLE_ADMIN');
            })
            ->forAll(function (int $_i, User $user) use ($entity) {
                $this->notificationService->send(
                    new NotificationMessage(
                        $user,
                        sprintf(
                            'A new ticket has been created by %s.',
                            $entity->getCreatedBy()->getName()
                        )
                    )
                );

                return true;
            });
    }
}