<?php
declare(strict_types=1);

namespace App\Security;

use App\Entity\Ticket;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class CommentCloseTicketVoter extends Voter
{
    public const COMMENT = 'comment';
    public const CLOSE = 'close';

    /** @var Security */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return $subject instanceof Ticket
            && $attribute === static::COMMENT || $attribute === static::CLOSE;
    }

    protected function voteOnAttribute($attribute, $ticket, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if (
            $this->security->isGranted('ROLE_USER', $user)
            && $user === $ticket->getCreatedBy()
        ) {
            return true;
        }

        if ($this->security->isGranted('ROLE_ADMIN', $user)) {
            return $user === $ticket->getAssignedTo() || $ticket->getStatus() === Ticket::STATUS_NEW;
        }

        return false;
    }
}