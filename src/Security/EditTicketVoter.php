<?php
declare(strict_types=1);

namespace App\Security;

use App\Entity\Ticket;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class EditTicketVoter extends Voter
{
    public const EDIT = 'edit';

    /** @var Security */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return $subject instanceof Ticket && $attribute === static::EDIT;
    }

    protected function voteOnAttribute($attribute, $ticket, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if (!$this->security->isGranted('ROLE_ADMIN', $user)) {
            return false;
        }

        return $user === $ticket->getAssignedTo() || $ticket->getAssignedTo() === null;
    }
}