<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        $defaultPassword = 'password';

        $admin = new User();
        $admin->setEmail('admin@example.com');
        $admin->setName('The Admin');
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            'password'
        ));
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $this->setReference('admin.1', $admin);

        $admin = new User();
        $admin->setEmail($faker->email);
        $admin->setName($faker->name);
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            $defaultPassword
        ));
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $this->setReference('admin.2', $admin);

        $admin = new User();
        $admin->setEmail($faker->email);
        $admin->setName($faker->name);
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            $defaultPassword
        ));
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $this->setReference('admin.3', $admin);

        $user = new User();
        $user->setEmail('user@example.com');
        $user->setName('The User');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'password'
        ));
        $manager->persist($user);
        $this->setReference('user.1', $user);

        $user = new User();
        $user->setEmail($faker->email);
        $user->setName($faker->name);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $defaultPassword
        ));
        $manager->persist($user);
        $this->setReference('user.2', $user);

        $user = new User();
        $user->setEmail($faker->email);
        $user->setName($faker->name);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $defaultPassword
        ));
        $manager->persist($user);
        $this->setReference('user.3', $user);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            NotificationFixture::class
        ];
    }
}
