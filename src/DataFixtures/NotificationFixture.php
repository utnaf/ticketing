<?php

namespace App\DataFixtures;

use App\Entity\NotificationType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class NotificationFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $sms = new NotificationType();
        $sms->setName('sms');
        $sms->setDescription('Send an SMS');
        $sms->setMandatory(false);
        $manager->persist($sms);

        $push = new NotificationType();
        $push->setName('push');
        $push->setDescription('Send a Push Notification');
        $push->setMandatory(false);
        $manager->persist($push);

        $email = new NotificationType();
        $email->setName('email');
        $email->setDescription('Send an Email');
        $email->setMandatory(true);
        $manager->persist($email);

        $manager->flush();
    }
}
