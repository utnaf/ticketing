<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Ticket;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class TicketFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        //
        // controlled fixtures for tests
        //

        /** @var User $testAdmin */
        $testAdmin = $this->getReference('admin.1');
        /** @var User $testUser */
        $testUser = $this->getReference('user.1');

        $ticketFromUser = new Ticket();
        $ticketFromUser->setCreatedBy($testUser);
        $commentForTicketFromUser = new Comment();
        $commentForTicketFromUser->setContent('this ticket if from a user, not assigned to anyone');
        $commentForTicketFromUser->setCreatedBy($testUser);
        $ticketFromUser->addComment($commentForTicketFromUser);
        $manager->persist($ticketFromUser);

        $ticketFromUserForAdmin = new Ticket();
        $ticketFromUserForAdmin->setCreatedBy($testUser);
        $ticketFromUserForAdmin->setAssignedTo($testAdmin);
        $commentForTicketFromUser = new Comment();
        $commentForTicketFromUser->setContent('this ticket if from a user, assigned to admin');
        $commentForTicketFromUser->setCreatedBy($testUser);
        $ticketFromUserForAdmin->addComment($commentForTicketFromUser);
        $manager->persist($ticketFromUserForAdmin);

        $ticketFromAdmin = new Ticket();
        $ticketFromAdmin->setCreatedBy($testAdmin);
        $commentForTicketFromAdmin = new Comment();
        $commentForTicketFromAdmin->setContent('this ticket if from an admin, not assigned to anyone');
        $commentForTicketFromAdmin->setCreatedBy($testAdmin);
        $ticketFromAdmin->addComment($commentForTicketFromAdmin);
        $manager->persist($ticketFromAdmin);

        $ticketFromAdminForAdmin = new Ticket();
        $ticketFromAdminForAdmin->setCreatedBy($testAdmin);
        $ticketFromAdminForAdmin->setAssignedTo($testAdmin);
        $commentForTicketFromAdminForAdmin = new Comment();
        $commentForTicketFromAdminForAdmin->setContent('this ticket if from an admin, assigned to admin');
        $commentForTicketFromAdminForAdmin->setCreatedBy($testAdmin);
        $ticketFromAdminForAdmin->addComment($commentForTicketFromAdminForAdmin);
        $manager->persist($ticketFromAdminForAdmin);

        $ticketFromAdminForAnotherAdmin = new Ticket();
        $ticketFromAdminForAnotherAdmin->setCreatedBy($testAdmin);
        $ticketFromAdminForAnotherAdmin->setAssignedTo($this->getReference('admin.2'));
        $commentForTicketFromAdminForAnotherAdmin = new Comment();
        $commentForTicketFromAdminForAnotherAdmin->setContent('this ticket if from an admin, assigned to another admin');
        $commentForTicketFromAdminForAnotherAdmin->setCreatedBy($testAdmin);
        $ticketFromAdminForAnotherAdmin->addComment($commentForTicketFromAdminForAnotherAdmin);
        $manager->persist($ticketFromAdminForAnotherAdmin);

        $ticketFromAnotherAdminForAnotherAdmin = new Ticket();
        $ticketFromAnotherAdminForAnotherAdmin->setCreatedBy($this->getReference('admin.2'));
        $ticketFromAnotherAdminForAnotherAdmin->setAssignedTo($this->getReference('admin.2'));
        $commentForTicketFromAnotherAdminForAnotherAdmin = new Comment();
        $commentForTicketFromAnotherAdminForAnotherAdmin->setContent('this ticket if from another admin, assigned to another admin');
        $commentForTicketFromAnotherAdminForAnotherAdmin->setCreatedBy($testAdmin);
        $ticketFromAnotherAdminForAnotherAdmin->addComment($commentForTicketFromAnotherAdminForAnotherAdmin);
        $manager->persist($ticketFromAnotherAdminForAnotherAdmin);

        //
        // dummy data
        //
        $admins = [
            $this->getReference('admin.2'),
            $this->getReference('admin.3'),
        ];
        $users = [
            $this->getReference('user.2'),
            $this->getReference('user.3'),
        ];

        $ticketToBeClosed = [];
        for ($i = 0; $i < 40; $i++) {
            $ticket = new Ticket();

            if ($faker->boolean(20)) {
                $ticket->setCreatedBy($faker->randomElement($admins));
            } else {
                $ticket->setCreatedBy($faker->randomElement($users));
            }

            if ($faker->boolean) {
                $ticket->setAssignedTo($faker->randomElement($admins));
            }

            $comment = new Comment();
            $comment->setContent($faker->realText());

            $ticket->addComment($comment);

            if ($faker->boolean) {
                $max = rand(0, 5);
                for ($commentIndex = 0; $commentIndex < $max; $commentIndex++) {
                    $additionalComment = new Comment();
                    $additionalComment->setContent($faker->realText());

                    $adminsToComment = $ticket->getAssignedTo() ? [$ticket->getAssignedTo()] : $admins;

                    if ($faker->boolean) {
                        $additionalComment->setCreatedBy($ticket->getCreatedBy());
                    } else {
                        $additionalComment->setCreatedBy($faker->randomElement($adminsToComment));
                    }

                    $ticket->addComment($additionalComment);
                }
            }

            if ($faker->boolean(35)) {
                $ticketToBeClosed[] = $ticket;
            }

            $manager->persist($ticket);
        }

        foreach ($ticketToBeClosed as $ticket) {
            $ticket->setStatus(Ticket::STATUS_CLOSED);

            if ($ticket->getAssignedTo() instanceof User) {
                $ticket->setClosedBy($faker->randomElement([$ticket->getAssignedTo(), $ticket->getCreatedBy()]));
            } else {
                $ticket->setClosedBy($ticket->getCreatedBy());
            }
            $manager->persist($ticket);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
