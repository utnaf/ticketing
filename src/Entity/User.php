<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ticket", mappedBy="createdBy", orphanRemoval=true)
     */
    private $createdTickets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ticket", mappedBy="assignedTo")
     */
    private $assignedTickets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="createdBy", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\NotificationType")
     */
    private $enabledNotificationTypes;

    public function __construct()
    {
        $this->createdTickets = new ArrayCollection();
        $this->assignedTickets = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->enabledNotificationTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function hasRole(string $role): bool
    {
        return in_array($role, $this->getRoles());
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getCreatedTickets(): Collection
    {
        return $this->createdTickets;
    }

    public function addCreatedTicket(Ticket $ticket): self
    {
        if (!$this->createdTickets->contains($ticket)) {
            $this->createdTickets[] = $ticket;
            $ticket->setCreatedBy($this);
        }

        return $this;
    }

    public function removeCreatedTicket(Ticket $ticket): self
    {
        if ($this->createdTickets->contains($ticket)) {
            $this->createdTickets->removeElement($ticket);
            // set the owning side to null (unless already changed)
            if ($ticket->getCreatedBy() === $this) {
                $ticket->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getAssignedTickets(): Collection
    {
        return $this->assignedTickets;
    }

    public function addAssignedTicket(Ticket $assignedTicket): self
    {
        if (!$this->assignedTickets->contains($assignedTicket)) {
            $this->assignedTickets[] = $assignedTicket;
            $assignedTicket->setAssignedTo($this);
        }

        return $this;
    }

    public function removeAssignedTicket(Ticket $assignedTicket): self
    {
        if ($this->assignedTickets->contains($assignedTicket)) {
            $this->assignedTickets->removeElement($assignedTicket);
            // set the owning side to null (unless already changed)
            if ($assignedTicket->getAssignedTo() === $this) {
                $assignedTicket->setAssignedTo(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setCreatedBy($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getCreatedBy() === $this) {
                $comment->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|NotificationType[]
     */
    public function getEnabledNotificationTypes(): Collection
    {
        return $this->enabledNotificationTypes;
    }

    public function addEnabledNotificationType(NotificationType $enabledNotificationType): self
    {
        if (!$this->enabledNotificationTypes->contains($enabledNotificationType)) {
            $this->enabledNotificationTypes[] = $enabledNotificationType;
        }

        return $this;
    }

    public function removeEnabledNotificationType(NotificationType $enabledNotificationType): self
    {
        if (
            $this->enabledNotificationTypes->contains($enabledNotificationType)
            && !$enabledNotificationType->isMandatory()
        ) {
            $this->enabledNotificationTypes->removeElement($enabledNotificationType);
        }

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setEmailNotification(LifecycleEventArgs $eventArgs)
    {
        /** @var NotificationType[] $notificationTypes */
        $notificationTypes = $eventArgs->getEntityManager()
            ->getRepository(NotificationType::class)
            ->findBy([
                'mandatory' => true
            ]);

        array_map(function (NotificationType $notificationType) {
            $this->addEnabledNotificationType($notificationType);
        }, $notificationTypes);
    }

    /**
     * @ORM\PreUpdate()
     */
    public function ensureEmailNotification(LifecycleEventArgs $eventArgs)
    {
        /** @var NotificationType[] $notificationTypes */
        $notificationTypes = $eventArgs->getEntityManager()
            ->getRepository(NotificationType::class)
            ->findBy([
                'mandatory' => true
            ]);

        array_map(function (NotificationType $notificationType) {
            $this->addEnabledNotificationType($notificationType);
        }, $notificationTypes);
    }
}
